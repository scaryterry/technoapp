Go to the root folder and perform "npm install" to install all the dependencies.
Install json-server (npm install json-server -g) as a global instance.
Go to jsondb folder and run json-server ( json-server --watch db.json -p 3001 )
From the root folder, run the app (npm start)
