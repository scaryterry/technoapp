import React, { Component } from "react";
import { Control, Form, Errors } from "react-redux-form";

const required = val => val && val.length;
const maxLength = len => val => !val || val.length <= len;
const minLength = len => val => val && val.length >= len;
const isNumber = val => !isNaN(Number(val));
const validEmail = val => /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(val);

class AddCustomer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <Form
        model="addCustomer"
        className=" form form-horizontal"
        onSubmit={values =>
          handleSubmit(values, this.props.resetAddForm, this.props.addCustomer)
        }
      >
        <div className="form-group col-sm-4 offset-sm-4">
          <label>
            <h3>Add a New Customer</h3>
          </label>
        </div>
        <div className="form-group col-sm-4 offset-sm-4">
          <Control.text
            model=".name"
            id="name"
            name="name"
            type="text"
            className="form-control"
            placeholder="Enter Name"
            validators={{
              required,
              minLength: minLength(3),
              maxLength: maxLength(15)
            }}
          />
          <Errors
            className="text-danger"
            model=".name"
            show="touched"
            messages={{
              required: "Required ",
              minLength: "Must be greater than 2 characters ",
              maxLength: "Must be 15 characters or less "
            }}
          />
        </div>
        <div className="form-group col-sm-4 offset-sm-4">
          <Control.text
            model=".email"
            id="email"
            name="email"
            type="email"
            className="form-control"
            placeholder="Enter Email Address"
            validators={{
              required,
              validEmail
            }}
          />
          <Errors
            className="text-danger"
            model=".email"
            show="touched"
            messages={{
              required: "Required ",
              validEmail: "Invalid Email Address "
            }}
          />
        </div>
        <div className="form-group col-sm-4 offset-sm-4">
          <Control.text
            model=".telnum"
            id="telnum"
            name="telnum"
            type="text"
            className="form-control"
            placeholder="Enter Telephone Number"
            validators={{
              required,
              minLength: minLength(3),
              maxLength: maxLength(15),
              isNumber
            }}
          />
          <Errors
            className="text-danger"
            model=".telnum"
            show="touched"
            messages={{
              required: "Required ",
              minLength: "Must be greater than 2 numbers ",
              maxLength: "Must be 15 numbers or less ",
              isNumber: "Must be a number "
            }}
          />
        </div>
        <div className="form-group col-sm-4 offset-sm-4">
          <button type="submit" className="btn btn-default">
            Submit
          </button>
        </div>
      </Form>
    );
  }
}

const handleSubmit = (values, resetAddForm, addCustomer) => {
  addCustomer(values.name, values.email, values.telnum);
  resetAddForm();
};

export default AddCustomer;
