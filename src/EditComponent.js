import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class EditCustomer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      email: "",
      telnum: "",
      touched: {
        name: false,
        email: false,
        telnum: false
      }
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleSubmit(event) {
    this.props.updateCustomer(
      this.props.customer.id,
      this.state.name,
      this.state.email,
      this.state.telnum
    );
    event.preventDefault();
    this.props.history.push("/home");
  }

  handleBlur = field => evt => {
    this.setState({
      touched: { ...this.state.touched, [field]: true }
    });
  };

  validate(name, email, telnum) {
    const errors = {
      name: "",
      email: "",
      telnum: ""
    };

    if (this.state.touched.name && name.length < 3)
      errors.name = "Name should be more than 3 characters";
    else if (this.state.touched.name && name.length > 25)
      errors.name = "Name cannot be more than 25 characters";

    const reg = /^\d+$/;
    if (this.state.touched.telnum && !reg.test(telnum))
      errors.telnum = "Tel. Number should contain only numbers";

    if (
      this.state.touched.email &&
      email.split("").filter(x => x === "@").length !== 1
    )
      errors.email = "Email should contain a @";

    return errors;
  }

  componentDidMount() {
    if (this.props.customer) {
      let name = this.props.customer.name;
      let email = this.props.customer.email;
      let telnum = this.props.customer.telnum;
      this.setState({
        name: name,
        email: email,
        telnum: telnum
      });
    }
  }

  render() {
    const { isLoading, errMess, customer } = this.props;

    const errors = this.validate(
      this.state.name,
      this.state.email,
      this.state.telnum
    );

    if (isLoading) {
      return <div>Loading...</div>;
    } else if (errMess) {
      return <div>Error: {errMess}</div>;
    } else if (customer.length !== null) {
      return (
        <form onSubmit={this.handleSubmit}>
          <div className="form-group col-sm-4 offset-sm-4">
            <label>
              <h3>Edit details of {customer.name}</h3>
            </label>
          </div>
          <div className="form-group col-sm-4 offset-sm-4">
            <input
              type="text"
              id="name"
              name="name"
              className="form-control"
              placeholder="Update Name"
              value={this.state.name}
              valid={errors.name === "" ? 1 : 0}
              invalid={errors.name !== "" ? 1 : 0}
              onBlur={this.handleBlur("name")}
              onChange={this.handleInputChange}
            />
            <label className="text-danger">{errors.name}</label>
          </div>
          <div className="form-group col-sm-4 offset-sm-4">
            <div>
              <input
                type="text"
                id="email"
                name="email"
                className="form-control"
                placeholder="Update E-mail"
                value={this.state.email}
                valid={errors.email === "" ? 1 : 0}
                invalid={errors.email !== "" ? 1 : 0}
                onBlur={this.handleBlur("email")}
                onChange={this.handleInputChange}
              />
              <label className="text-danger">{errors.email}</label>
            </div>
          </div>
          <div className="form-group col-sm-4 offset-sm-4">
            <div>
              <input
                type="text"
                id="telnum"
                name="telnum"
                className="form-control"
                placeholder="Update Tel. Number"
                value={this.state.telnum}
                valid={errors.telnum === "" ? 1 : 0}
                invalid={errors.telnum !== "" ? 1 : 0}
                onBlur={this.handleBlur("telnum")}
                onChange={this.handleInputChange}
              />
              <label className="text-danger">{errors.telnum}</label>
            </div>
          </div>
          <div className="form-group col-sm-4 offset-sm-4">
            <div>
              <button
                type="submit"
                className="btn btn-default"
                disabled={errors.email || errors.name || errors.telnum}
              >
                Update
              </button>
            </div>
          </div>
        </form>
      );
    }
  }
}

export default withRouter(EditCustomer);
