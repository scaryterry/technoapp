import React, { Component } from "react";
import { Link } from "react-router-dom";

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { isLoading, errMess, customers } = this.props.customers;
    if (isLoading) {
      return <span>Loading the data...</span>;
    } else if (errMess) {
      return <span>Error: {errMess}</span>;
    } else if (customers.length !== 0) {
      return (
        <RenderLists
          customers={customers}
          deleteCustomer={this.props.deleteCustomer}
        />
      );
    } else {
      return <span>No customers found</span>;
    }
  }
}

const RenderLists = props => {
  const { customers, deleteCustomer } = props;
  return (
    <table className="table">
      <thead>
        <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>Edit</th>
          <th>Delete</th>
        </tr>
      </thead>
      <tbody>
        {customers.map(customer => {
          return (
            <tr key={customer.id}>
              <td>{customer.name}</td>
              <td>{customer.email}</td>
              <td>{customer.telnum}</td>
              <td>
                <Link
                  className="button btn-info btn-sm"
                  to={`/edit/${customer.id}`}
                >
                  Edit
                </Link>
              </td>
              <td>
                <button
                  className="button btn-danger btn-sm"
                  onClick={() => {
                    deleteCustomer(customer.id);
                  }}
                >
                  Delete
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default List;
