import React, { Component } from "react";
import { Switch, Route, Redirect, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { actions } from "react-redux-form";

import Header from "./HeaderComponent";
import AddCustomer from "./AddComponent";
import EditCustomer from "./EditComponent";
import List from "./ListComponent";
import {
  fetchCustomers,
  addCustomer,
  deleteCustomer,
  updateCustomer
} from "./redux/ActionCreators";

const mapStateToProps = state => {
  return {
    customers: state.customers
  };
};

const mapDispatchToProps = dispatch => ({
  fetchCustomers: () => {
    dispatch(fetchCustomers());
  },
  resetAddForm: () => {
    dispatch(actions.reset("addCustomer"));
  },
  addCustomer: (name, email, telnum) => {
    dispatch(addCustomer(name, email, telnum));
  },
  deleteCustomer: id => {
    dispatch(deleteCustomer(id));
  },
  updateCustomer: (id, name, email, telnum) => {
    dispatch(updateCustomer(id, name, email, telnum));
  }
});

class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.fetchCustomers();
  }

  render() {
    const EditId = ({ match }) => {
      return (
        <EditCustomer
          customer={
            this.props.customers.customers.filter(
              customer => customer.id === parseInt(match.params.custId, 10)
            )[0]
          }
          isLoading={this.props.customers.isLoading}
          ErrMess={this.props.customers.errMess}
          updateCustomer={this.props.updateCustomer}
        />
      );
    };

    return (
      <React.Fragment>
        <Header />
        <Switch>
          <Route
            path="/home"
            component={() => (
              <List
                customers={this.props.customers}
                deleteCustomer={this.props.deleteCustomer}
              />
            )}
          />
          <Route path="/edit/:custId" component={EditId} />
          <Route
            path="/add"
            component={() => (
              <AddCustomer
                addCustomer={this.props.addCustomer}
                resetAddForm={this.props.resetAddForm}
              />
            )}
          />
          <Redirect to="/home" />
        </Switch>
      </React.Fragment>
    );
  }
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Main)
);
