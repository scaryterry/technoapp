import * as ActionTypes from "./ActionTypes";
import { baseUrl } from "../shared/baseUrl";

export const customersLoading = () => ({
  type: ActionTypes.CUSTOMERS_LOADING
});

export const customersFailed = errmess => ({
  type: ActionTypes.CUSTOMERS_FAILED,
  payload: errmess
});

export const addCustomers = customers => ({
  type: ActionTypes.ADD_CUSTOMERS,
  payload: customers
});

export const fetchCustomers = () => dispatch => {
  dispatch(customersLoading(true));

  return fetch(baseUrl + "customers")
    .then(
      response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error(
            "Error " + response.status + ": " + response.statusText
          );
          error.response = response;
          throw error;
        }
      },
      error => {
        var errmess = new Error(error.message);
        throw errmess;
      }
    )
    .then(response => response.json())
    .then(customers => dispatch(addCustomers(customers)))
    .catch(error => dispatch(customersFailed(error.message)));
};

export const addCustomer = (name, email, telnum) => dispatch => {
  const newCustomer = {
    name,
    email,
    telnum
  };

  return fetch(baseUrl + "customers", {
    method: "POST",
    body: JSON.stringify(newCustomer),
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  })
    .then(
      response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error(
            "Error " + response.status + ": " + response.statusText
          );
          error.response = response;
          throw error;
        }
      },
      error => {
        var errmess = new Error(error.message);
        throw errmess;
      }
    )
    .then(response => response.json())
    .then(response => {
      dispatch(addCustomers(response));
      alert("Customer Added!\n");
    })
    .then(() => dispatch(fetchCustomers()))
    .catch(error => {
      alert("Unable to Add Customer\nError: " + error.message);
    });
};

export const deleteCustomer = id => dispatch => {
  return fetch(baseUrl + "customers/" + id, {
    method: "DELETE"
  })
    .then(
      response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error(
            "Error " + response.status + ": " + response.statusText
          );
          error.response = response;
          throw error;
        }
      },
      error => {
        var errmess = new Error(error.message);
        throw errmess;
      }
    )
    .then(response => response.json())
    .then(() => dispatch(fetchCustomers()))
    .catch(error => {
      alert("Unable to Delete Customer\nError: " + error.message);
    });
};

export const updateCustomer = (id, name, email, telnum) => dispatch => {
  const updateCustomer = {
    name,
    email,
    telnum
  };

  return fetch(baseUrl + "customers/" + id, {
    method: "PUT",
    body: JSON.stringify(updateCustomer),
    headers: {
      "Content-Type": "application/json"
    },
    credentials: "same-origin"
  })
    .then(
      response => {
        if (response.ok) {
          return response;
        } else {
          var error = new Error(
            "Error " + response.status + ": " + response.statusText
          );
          error.response = response;
          throw error;
        }
      },
      error => {
        var errmess = new Error(error.message);
        throw errmess;
      }
    )
    .then(response => response.json())
    .then(() => dispatch(fetchCustomers()))
    .catch(error => {
      alert("Unable to Update Customer\nError: " + error.message);
    });
};
