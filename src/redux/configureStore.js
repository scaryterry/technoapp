import { applyMiddleware, combineReducers, createStore } from "redux";
import { createForms } from "react-redux-form";
import { Customers } from "./customers";
import logger from "redux-logger";
import thunk from "redux-thunk";
import { InitialData } from "./forms";

export const ConfigureStore = () => {
  const store = createStore(
    combineReducers({
      customers: Customers,
      ...createForms({
        addCustomer: InitialData,
        editCustomer: InitialData
      })
    }),
    applyMiddleware(thunk, logger)
  );

  return store;
};
